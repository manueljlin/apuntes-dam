# Regex
!!! info "Apuntes específicos a JavaScript/ECMAscript. Es posible que cambien algunas cosas en otros lenguajes."
Las expresiones regulares o *regex* son esencialmente una secuencia de caracteres que especifícan qué buscar en un texto. Sirven

## Delimitadores
Placeholder

## Flags
Activan y desactivan diferentes funciones

### Flags principales
- Global `g`
    * Encuentra todas las coincidencias en vez de solo la primera.
- Multiline `m`
    * Sigue buscado tras la primera línea, si se usa `^` o `$`.
- Insensitive `i`
    * Buscar tanto mayúsculas y minúsculas. Ignora `[a-zA-Z]`, porque hace lo mismo.
- Sticky `y`
    * Placeholder

### Otros flags menos usados
- Unicode `u`
    * Activa todas las funciones Unicode
- Single Line `s`
    * Placeholder
    !!! warning "Esto es un draft, por lo que puede que no funcione todavía o cambie en el futuro"
- Indices `d`
    * Placeholder

---

## Ejemplos

``` title="Teléfono con prefijo opcional y paréntesis: `\(\(?\+\d{2}\)?\d{9}`" hl_lines="1 1"
+34 657788778
```

``` title="Dirección de una página web: `'https?://(www\.)?.+\..{2,}$`" hl_lines="1 2"
http://www.misitio.com
https://www.misitio.com
http://www.misitio.commmm
https://www.misitio.comhttps://www.misitio.com
```

---

## Enlaces útiles
- [Regex101](https://regex101.com/), que permite previsualizar los regex y ver las diferencias entre los lenguajes de programación.
