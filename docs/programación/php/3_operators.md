# Operadores

## Asignación por referencia
Lo que ocurre por defecto al modificar una variable dentro de una función es que solo se cambiará el valor dentro de la propia función, y fuera se mantendrá el valor original. Sin embargo, la asignación por referencia sí puede modificar el valor original.

Se añade un ampersand `&` antes del `$nombre_variable` (en este caso sería `&$nombre_variable`) para que se asigne por referencia.

```php
// Asignado por referencia
function calcular(&$a){
    $a++;
}
$a=5;
calcular($a); // Imprime 6, sí modifica el valor original
echo $a;      // Imprime 6, valor modificado anteriormente


// Asignado por valor
function calcular($a){
    $a++;
}
$a=5;
calcular($a); // Imprime 6, no modifica el valor original
echo $a;      // Imprime 5
```