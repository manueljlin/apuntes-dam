# Guía de estilo recomendada
Una adaptación corta del PDF del Classroom. Recomiendo que solamente te fijes en los ejemplos e ignores la descripciones porque a menudo lían más que lo que realmente es.

[TOC]


## Delimitadores de bloque

```php
// Sí
<?php
?>

// No
<?
?>
```


## Comentarios

```php
// Estilo de comentario de una sola línea

/*
Estilo de comentario de varias líneas:
    1. Una línea 
    2. Otra línea 
*/
```


## Bloques de comentario iniciales
Los programas deben de incluir al principio docblocks

```php
<?php
/**
 * Descripción breve
 * 
 * Descripción extensa (opcional)
 * 
 * @author Fulanito de Tal <fulanito@example.com>
 * @copyright 2007 Fulanito de Tal
 * @license https://fsf.com/licensing/licenses/gpl.txt GPL v2 or later
 * @version 2007-02-06
 * @link https://www.example.com
*/
El código va aquí después...
?>
```


## Longitud de línea
No debe superar los 85 caracteres por línea. Se usa para indentar cuatro espacios.


## Instrucciones por línea
Se aconseja solo una instrucción por línea.


## Espacios en blanco
Después de una coma, operadores binarios y unarios poner un espacio en blanco.

```php
// Coma
$var = foo($bar, $cel, $ona);

// Operador binario (+, -, *, /, ., ==, &&, ||, ?, :, etc)
$cmTotal = 100000 * $km + 100 * $m + $cm;

// Operador unario (!, ++, --, etc)
$correcto = !$error;
```


## Líneas en blanco
Se aconseja dividir partes distintas del programa con líneas en blanco


## Nombres de variables
Se deben escribir en camelCase, es decir, todas las palabras comienzan con una letra mayúscula menos la primera.
```php
$nombre;
$nombreAlumno;
$nombreProfesor;
```


## Alineación de definiciones de variables
Al definir varias variables, alinéalo con espacios en blanco para que sea más legible

```php
// Sí
$nombre         = 'Fulano';
$nombreAlumno   = 'Mengano';
$nombreProfesor = 'Zutano';

// No
$nombre = 'Fulano';
$nombreAlumno = 'Mengano';
$nombreProfesor = 'Zutano';
```


## Constantes
Escribir en mayúsculas y separando palabras con guiones bajos (_)
```php
define('FOO',     'cosa');
define('FOO2',    'otra cosa');
define('FOO_BAR', 'otra cosa más');
```


## Sangrado
Usar cuatro espacios de sangrado. Acumular sangrado en estructuras anidadas.

```php
if (condicion1) {
    accion1;
} else {
    accion2;
    if (condicion3) {
        accion3;
    } else {
        accion4;
    }
}
```


## Estructuras de control
Deben tener espacio entre la palabra reservada y el paréntesis inicial, para distinguirlas de las funciones.

En los bloques de sentencias se usan siempre llaves. La llave de apertura se pone en el final de la línea y el de cierre al principio.

```php
// Ejemplo 1
if (condicion1 || condicion2) {
    accion1;
} elseif (condicion3 && condicion4) {
    accion2;
} else {
    accionpredeterminada;
}

// Ejemplo 2
switch (condicion) {
    case 1:
        accion1;
        break;
    case 2:
        accion2;
        break;
    default:
        accionpredeterminada;
    break;
}
```


## Expresiones lógicas
Para comparar variables booleanas, usar !$bool o $bool en vez de $bool == true o $bool == false.

```php
// Sí
if ($correcto) {
    print 'Es correcto';
}
if (!$correcto) {
    print 'No es correcto';
}

// No
if ($correcto == true) {
    print 'Es correcto';
}
if ($correcto == false) {
    print 'No es correcto';
}
```

Al concatenar expresiones lógicas sencillas, no es necesario escribir cada expresión entre
paréntesis.

```php
if {$numero < 0 || $numero > 100} {
    print 'El número no está entre 0 y 100'
}
```


## Definición de funciones
Usar el estilo BSD/Allman. ¿Por qué? A saber.

```php
// Sí: los corchetes de function {} se ponen como en C#
function fooFuncion($arg1, $arg2 = "")
{
    if (condicion) {
        sentencia;
    }
    return $val;
}

// No: no poner el estilo que usamos en el resto de cosas.
function fooFuncion($arg1, $arg2 = "") {
    if (condicion) {
        sentencia;
    }
    return $val;
}
```

Los argumentos con valores predeterminados se deben colocar al final de la lista de
argumentos.

Las funciones deben devolver algún valor.


## Llamadas a funciones
No debe haber espacios entre el nombre de la función, paréntesis inicial y el primer argumento. Debe haber espacios tras las comas que separen argumentos. No debe haber
espacios entre el último argumento, el paréntesis final y el punto y coma.

```php
$var = foo($bar, $cel, $ona);
```


## Bibliotecas
Para incluir bibliotecas que no dependen de ninguna condición, hay que usar `require_once` y no usar paréntesis alrededor del nombre de la biblioteca.

```php
    require_once './subdirectorio/biblioteca.php';
```


# Direcciones
- https://pear.php.net/manual/en/standards.php
- https://www.php-fig.org/psr/psr-1/
- https://www.php-fig.org/psr/psr-12/


# Extensión VSCode
- https://marketplace.visualstudio.com/items?itemName=Sophisticode.php-formatter
