---
title: Instalar el software 
---

# Instalando el software necesario

## Windows
Si usas Windows, solamente necesitas instalar Laragon [desde aquí](https://laragon.org/download/#Edition). Recuerda instalar la versión completa o *"Full"*.

## Linux y macOS
Vamos a instala XAMPP desde [aquí](https://www.apachefriends.org/download.html), teniendo cuidado de descargar la versión que corresponda al que usamos en la clase. También tiene una interfaz similar a la de Laragon en macOS, y trae todas las dependencias necesarias.

Aunque XAMPP usa MariaDB en vez de MySQL, son completamente compatibles entre sí.

=== "Instalando en macOS"
    1. Abre el archivo DMG
    2. Haz doble clic en la imagen para instalarlo

    !!! info "Se instalará en el directorio /Aplicaciones/XAMMP"

=== "Instalando en Linux"
    1. Cambia los permisos del instalador con `chmod 755 xampp-linux-*-installer.run`
    2. Ejecuta el instalador con `sudo ./xampp-linux-*-installer.run`

    !!! info "Se instalará en el directorio /opt/lampp"